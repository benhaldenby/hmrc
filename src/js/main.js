
// Device detection
detectDevice = {
  isMobile: function() {
    return navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/Android/i) && navigator.userAgent.match(/Mobile/i) || navigator.userAgent.match(/Blackberry/i) || navigator.userAgent.match(/BB10/i) || navigator.userAgent.match(/Nokia/i) || navigator.userAgent.match(/Windows Phone/i)?!0 : !1;
  },
  isTablet: function() {
    return navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/Nexus/i) || navigator.userAgent.match(/Playbook/i)?!0 : !1;
  },
  isSafari: function() {
    return !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
  },
  isChrome: function() {
    return !!navigator.userAgent.match(/Version\/[\d\.]+.*Chrome/);
  }
};



$(document).ready(function($) {

  // 

});