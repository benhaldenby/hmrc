module.exports = function(grunt) {

  var globalConfig = {
    src: 'src',
    dest: 'dist'
  };

  // All configuration goes here 
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    globalConfig: globalConfig,
    
    // Copy IE polyfills to /js
    copy: {
      main: {
        files: [
          {expand: true, flatten: true, src: ['<%= globalConfig.src %>/js/ie/*'],                        dest: '<%= globalConfig.dest %>/assets/js/vendor/', filter: 'isFile'},
          {expand: true, flatten: true, src: ['<%= globalConfig.src %>/js/vendor/jquery-2.1.0.min.js'],  dest: '<%= globalConfig.dest %>/assets/js/vendor/', filter: 'isFile'},
          {expand: true, flatten: true, src: ['<%= globalConfig.src %>/fonts/*'],                        dest: '<%= globalConfig.dest %>/assets/fonts/', filter: 'isFile'},
          {expand: true, flatten: true, src: ['<%= globalConfig.src %>/index.html'],                     dest: '<%= globalConfig.dest %>', filter: 'isFile'}
        ],
      },
    },

    //Asset clean up
    imagemin: {
      options: {
        optimizationLevel: 3,
        tolerance: 1000
      },
      dynamic: {
        files: [{
          expand: true,
          cwd: '<%= globalConfig.src %>/img/',
          src: ['*.{png,jpg,gif}', '**/*.{png,jpg,gif}'],
          dest: '<%= globalConfig.dest %>/assets/img/'
        }]
      }
    },

    uglify: {
      dist: {
        files: {
          '<%= globalConfig.dest %>/assets/js/main.min.js': 
          [ 
            '<%= globalConfig.src %>/js/vendor/imagesloaded.js',
            '<%= globalConfig.src %>/js/main.js'
          ]
        }
      },
      dev: {
        options:{
          sourceMap: true,
          beautify: false,
          compress: true,
          mangle: false
        },
        files: {
          '<%= globalConfig.dest %>/assets/js/main.min.js': 
          [ 
            '<%= globalConfig.src %>/js/vendor/imagesloaded.js',
            '<%= globalConfig.src %>/js/main.js'
          ]
        }
      }
    },
    
    // Image spriting
    // sprite:{
    //   all: {
    //   src: 'img/raw/*.png',
    //   retinaSrcFilter: 'img/raw/*-2x.png',
    //   dest: 'img/sprite.png',
    //   retinaDest: 'img/sprite-2x.png',
    //   destCss: 'css/_spritesmith.scss'
    //   }
    // },

    // SVG

    // SVG optimisation
    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= globalConfig.src %>/svg/raw',
          src: ['*.svg'],
          dest: '<%= globalConfig.src %>/svg/optimised'
        }]
      },
      options: {
        plugins: [
            { removeViewBox: false },               // don't remove the viewbox attribute from the SVG
            { removeEmptyAttrs: false }             // don't remove Empty Attributes from the SVG
        ]
      }
    },

    // Grunticon Icons
    grunticon: {
      icons: {
        files: [{
          expand: true,
          cwd: '<%= globalConfig.src %>/svg/optimised',
          src: ['*.svg', '*.png'],
          dest: "assets/css/icons"
        }],
        options: {
          cssprefix: '.icon--'
        }
      }
    },

    // Merge SVGs from a folder into one sprited document
    svgstore: {
      options: {
        prefix : 'icon-', // This will prefix each <g> ID
        svg : {
          'xmlns:sketch' : 'http://www.bohemiancoding.com/sketch/ns',
          'display': 'none',
          'width': '0',
          'height': '0',
          'style': 'display:none'
        }
      },
      "default": {
        files: {
          '<%= globalConfig.dest %>/assets/svg/svgdefs.svg': ['<%= globalConfig.src %>/svg/optimised/*.svg']
        },
      }
    },

    // Create PNG fallbacks from SVGs
    svg2png: {
      dist: {
        files: [{ 
          //flatten: true,
          cwd: '<%= globalConfig.src %>/svg/optimised/', 
          src: ['*.svg'], 
          dest: '<%= globalConfig.dest %>/assets/img/icons/' }
        ]
      }
    },
    
    //CSS Generation
    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        options: {
          outputStyle: 'compressed'
        },
        files: {
          '<%= globalConfig.dest %>/assets/css/legacy-ie.min.css': '<%= globalConfig.src %>/scss/legacy-ie.scss',
          '<%= globalConfig.dest %>/assets/css/main.min.css': '<%= globalConfig.src %>/scss/main.scss'
        }
      },
      dev: {
        options: {
          outputStyle: 'compressed'
        },
        files: {
          '<%= globalConfig.dest %>/assets/css/legacy-ie.min.css': '<%= globalConfig.src %>/scss/legacy-ie.scss',
          '<%= globalConfig.dest %>/assets/css/main.min.css': '<%= globalConfig.src %>/scss/main.scss'
        }
      }
    },

    autoprefixer: {
      options: {
        map: true
      },

      // prefix the specified file
      single_file: {
        options: {
        // Target-specific options go here.
        },
        src: '<%= globalConfig.dest %>/assets/css/main.min.css',
        dest: '<%= globalConfig.dest %>/assets/css/main.min.css'
      },
    },
    csso: {
      compress: {
        options: {
          report: 'gzip'
        },
        files: {
          '<%= globalConfig.dest %>/assets/css/main.min.css': ['<%= globalConfig.dest %>/assets/css/main.min.css']
        }
      },
      restructure: {
        options: {
          report: 'min'
        },
        files: {
          '<%= globalConfig.dest %>/assets/css/main.min.css': ['<%= globalConfig.dest %>/assets/css/main.min.css']
        }
      }
    },
    
    // Linting
    csslint: {
      check: {
        src: [
          '<%= globalConfig.dest %>/assets/css/*.css'
        ]
      }
    },  
    jshint: {
      all: ['Gruntfile.js', '<%= globalConfig.src %>/js/*.js']
    },

    
    // Regeneration/Change Detection
    watch: {
      options: {
        livereload: false,
      },
      scripts: {
        files: ['<%= globalConfig.src %>/js/*.js', '<%= globalConfig.src %>/js/**/*.js'],
        tasks: ['uglify'/*,'jshint'*/],
        options: {
          spawn: false,
        },
      },
      css: {
        files: ['<%= globalConfig.src %>/scss/main.scss', '<%= globalConfig.src %>/scss/*/*.scss'],
        tasks: ['sass:dist', 'autoprefixer', /* 'csso', 'csslint'*/],
        options: {
          spawn: false,
        }
      },
      html: {
        files: ['<%= globalConfig.src %>/*.html'],
        options: {
          spawn: false,
        }
      }
    },

    // Live refresh browsers and keep in sync
    browserSync: {
      bsFiles: {
        src : [
          '<%= globalConfig.dest %>/assets/css/*.css',
          '<%= globalConfig.dest %>/assets/js/*.js',
          '*.html'
        ]
      },
      options: {
        watchTask: true,
        server: {
          baseDir: "dist"
        }
      }
    },  

    //Run build and serve at the same time.
    serve: {
      options: {
        port: 9000
      }
    },

    newer: {
      options: {
        tolerance: 1000
      }
    },

  });


  // Plugin List

  // Move static assets
  grunt.loadNpmTasks('grunt-contrib-copy');

  // Asset clean up
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  
  // CSS generation
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-csso');
  
  // Linting
  grunt.loadNpmTasks('grunt-contrib-csslint');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  
  // Updating
  grunt.loadNpmTasks('grunt-newer');

  // Regeneration/Change Detection
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Running
  //grunt.loadNpmTasks('grunt-serve');

  // Browser Syncing 
  grunt.loadNpmTasks('grunt-browser-sync');

  // Tasks
  grunt.registerTask('default', ['newer:imagemin', 'copy', 'uglify',      'sass',      'autoprefixer', /*'csso'*/              'watch']);
  grunt.registerTask('dist',    ['newer:imagemin', 'copy', 'uglify:dist', 'sass:dist', 'autoprefixer', /*'csso'*/              'watch']);
  grunt.registerTask('dev',     ['newer:imagemin', 'copy', 'uglify:dev',  'sass:dev',  'autoprefixer', /*'csslint',*/ 'jshint', 'browserSync', 'watch']);

};